<?php

/**
 * @file
 *   Module functions for the relwmls module.
 * @defgroup relwmls
 * @ingroup relwmls
 */

/**
 * relwmls_mls_to_ulisting()
 */
function relwmls_mls_to_ulisting() {
  $result = db_query("SELECT * FROM {relwmls_mls_listing}");
  while ($data = db_fetch_object($result)) {
    $node->nid = $data->nid;
    $node->mls_num = $data->mls_listing_id;
    $node->country_id = 125; // USA
    $node->prov_id = 9; // FL
    $node->address = $data->street_number .' '. $data->street_direction .' '. $data->street_name .' '. $data->street_type;
    $node->city_name = $data->city;
    $node->postal_code = $data->zip_code .'-'. $data->zip_plus4;
    $node->price = $data->sale_price;
    $node->year_built = $data->year_built;
    $node->bedroom = $data->bedrooms;
    $node->bathroom_full = $data->baths_full;
    $node->bathroom_half = $data->baths_half;
    $node->floor_space = $data->building_square_footage;
    $node->land_size = $data->lot_square_footage;
    $node->storeys = '';
    $node->maintenance_fee = $data->hoa_fees;
    $node->is_featured = 0;
    $node->list_date = $data->listing_date;
    $node->timestamp = $created;
    $node->lat = $data->latitude;
    $node->lon = $data->longitude;
    $node->no_map = 0;
    $node->is_manual_map = 0;
    $node->currency = '$';
    $node->measurement_unit = 0;
    $node->rets_id = 0;
    $node->expiredate = $data->listing_expiration_date;
    db_query("INSERT INTO {ulisting_listing} (
      `nid`,
      `mls_num`,
      `country_id`,
      `prov_id`,
      `address`,
      `city_name`,
      `postal_code`,
      `price`,
      `year_built`,
      `bedroom`,
      `bathroom_full`,
      `bathroom_half`,
      `floor_space`,
      `land_size`,
      `storeys`,
      `maintenance_fee`,
      `is_featured`,
      `list_date`,
      `timestamp`,
      `lat`,
      `lon`,
      `no_map`,
      `is_manual_map`,
      `currency`,
      `measurement_unit`,
      `rets_id`,
      `expiredate`
    ) VALUES (%d,'%s',%d,%d,'%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s',%d,%d,%d)",
      $node->nid,
      $node->mls_num,
      $node->country_id,
      $node->prov_id,
      $node->address,
      $node->city_name,
      $node->postal_code,
      $node->price,
      $node->year_built,
      $node->bedroom,
      $node->bathroom_full,
      $node->bathroom_half,
      $node->floor_space,
      $node->land_size,
      $node->storeys,
      $node->maintenance_fee,
      $node->is_featured,
      $node->list_date,
      $node->timestamp,
      $node->lat,
      $node->lon,
      $node->no_map,
      $node->is_manual_map,
      $node->currency,
      $node->measurement_unit,
      $node->rets_id,
      $node->expiredate
    );
  }
  drupal_set_message('uListings created');
  drupal_goto('');
}

