<?php

/**
 * @file
 *   Form function for the relwmls module.
 * @ingroup relwmls
 */

/**
 * Implementation of hook_form()
 */
function relwmls_form(&$node, $form_state) {
  $form['feature_codes'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  $weight = 100;
//
  $weight += 1;
  $form['mls_id'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS ID'),
    '#description' => 'FAR provided ID for MLS',
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_state_id'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS State ID'),
    '#description' => 'The State of the MLS',
    '#size' => 5,
    '#maxlength' => 3,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_listing_id'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Listing ID'),
    '#description' => 'The unique listing identifier for the property',
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['tln_firm_id'] = array(
    '#type' => 'textfield',
    '#title' => t('TLN Firm ID'),
    '#description' => 'The Living Network (TLN) identification for the of listing office',
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_office_name'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Office Name'),
    '#description' => 'Listing Office\'s name',
    '#size' => 122,
    '#maxlength' => 120,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_office_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Office Phone'),
    '#description' => 'Office\'s phone number (include area code)',
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['tln_realtor_id'] = array(
    '#type' => 'textfield',
    '#title' => t('TLN Realtor ID'),
    '#description' => 'The Living Network (TLN) identification for the listing agent',
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_agent_name'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Agent Name'),
    '#description' => 'Listing Agent\'s name',
    '#size' => 52,
    '#maxlength' => 50,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_agent_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Agent Phone'),
    '#description' => 'Listing Agent\'s phone number (include area code)',
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['listing_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Listing Date'),
    '#description' => t('Format MM/DD/YYYY'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['listing_expiration_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Listing Expiration Date'),
    '#description' => t('Format MM/DD/YYYY'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['sold_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Sold Date'),
    '#description' => t('Format MM/DD/YYYY'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['available_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Available Date'),
    '#description' => t('Format MM/DD/YYYY'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $property_type_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Property type codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d", $vid);
  while ($data = db_fetch_object($result)) {
    $property_type_codes[] = $data->name;
  }
  $form['property_type'] = array(
    '#type' => 'select',
    '#title' => 'Property Type',
    '#default_value' => variable_get('relwmls_property_type_code', t('Single Family')),
    '#options' => drupal_map_assoc($property_type_codes),
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['prop_type_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Property Type Description'),
    '#description' => t('Short Description associated with the property'),
    '#size' => 52,
    '#maxlength' => 50,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['remarks'] = array(
    '#type' => 'textarea',
    '#title' => t('Remarks'),
    '#description' => t('2000 character maximum'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $status_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Status codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d", $vid);
  while ($data = db_fetch_object($result)) {
    $status_codes[] = $data->name;
  }
  $form['status_code'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => variable_get('relwmls_status_code', t('Active')),
    '#options' => drupal_map_assoc($status_codes),
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['sale_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Sale Price'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['sold_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Sold Price'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
/*
  $weight += 1;
  $state_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'State codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d", $vid);
  while ($data = db_fetch_object($result)) {
    $state_codes[] = $data->name;
  }
  $form['state_codes'] = array(
    '#type' => 'select',
    '#title' => 'State',
    '#default_value' => variable_get('relwmls_state_code', 'Florida'),
    '#options' => drupal_map_assoc($state_codes),
    '#weight' => $weight,
  );
*/
//
  $weight += 1;
  $form['street_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Street Number'),
    '#description' => t('Street Number'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['street_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Street Name'),
    '#description' => t('Street Name'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['street_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Street Type'),
    '#description' => t('Street Type (RD, ST, etc.)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['street_direction'] = array(
    '#type' => 'textfield',
    '#title' => t('Street Direction'),
    '#description' => t('Street Direction (N, E, S, W, etc.)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['unit_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Unit Number'),
    '#description' => t('Apartment or Unit number'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#description' => t('Format: 99.9999999999999999999999 (no commas, dollar signs)'),
    '#size' => 27,
    '#maxlength' => 25,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#description' => t('Format: 99.9999999999999999999999 (no commas, dollar signs)'),
    '#size' => 27,
    '#maxlength' => 25,
    '#weight' => $weight,
  );
/*
//
  $weight += 1;
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City name'),
    '#description' => t('City name'),
    '#size' => 27,
    '#maxlength' => 25,
    '#weight' => $weight,
  );
*/
//
  $weight += 1;
  $city_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'City codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d", $vid);
  while ($data = db_fetch_object($result)) {
    $city_codes[] = $data->name;
  }
  $form['city'] = array(
    '#type' => 'select',
    '#title' => 'City',
    '#default_value' => variable_get('relwmls_city_code', 'Orlando'),
    '#options' => drupal_map_assoc($city_codes),
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['zip_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code'),
    '#description' => t('Zip Code'),
    '#size' => 7,
    '#maxlength' => 5,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['zip_plus4'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Plus 4'),
    '#description' => t('Zip Plus 4'),
    '#size' => 6,
    '#maxlength' => 4,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_area'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Area'),
    '#description' => t('MLS Area'),
    '#size' => 27,
    '#maxlength' => 25,
    '#weight' => $weight,
  );
/*
//
  $weight += 1;
  $form['county'] = array(
    '#type' => 'textfield',
    '#title' => t('County name'),
    '#description' => t('County name'),
    '#size' => 27,
    '#maxlength' => 25,
    '#weight' => $weight,
  );
*/
//
  $weight += 1;
  $county_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'County codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d", $vid);
  while ($data = db_fetch_object($result)) {
    $county_codes[] = $data->name;
  }
  $form['county'] = array(
    '#type' => 'select',
    '#title' => 'County',
    '#default_value' => variable_get('relwmls_county_code', 'Orange'),
    '#options' => drupal_map_assoc($county_codes),
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['subdivision'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdivision'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['community_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Community Name'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['year_built'] = array(
    '#type' => 'textfield',
    '#title' => t('Year Built'),
    '#description' => t('Format: 9999 (no commas, dollar signs)'),
    '#size' => 6,
    '#maxlength' => 4,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['acres'] = array(
    '#type' => 'textfield',
    '#title' => t('Acres'),
    '#description' => t('Format: 999999.99 (no commas, dollar signs)'),
    '#size' => 11,
    '#maxlength' => 9,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['lot_dimensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Lot Dimensions'),
    '#description' => t('Lot Dimensions'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['lot_square_footage'] = array(
    '#type' => 'textfield',
    '#title' => t('Lot Square Footage'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['lot_square_footage_land'] = array(
    '#type' => 'textfield',
    '#title' => t('Lot Square Footage Land'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['building_square_footage'] = array(
    '#type' => 'textfield',
    '#title' => t('Building Square Footage'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['bedrooms'] = array(
    '#type' => 'textfield',
    '#title' => t('Bedrooms'),
    '#description' => t('Format: 99 (no commas, dollar signs)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['baths_total'] = array(
    '#type' => 'textfield',
    '#title' => t('Baths Total'),
    '#description' => t('Format: 999.99 (no commas, dollar signs)'),
    '#size' => 8,
    '#maxlength' => 6,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['baths_full'] = array(
    '#type' => 'textfield',
    '#title' => t('Baths Full'),
    '#description' => t('Format: 99 (no commas, dollar signs)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['baths_half'] = array(
    '#type' => 'textfield',
    '#title' => t('Baths Half'),
    '#description' => t('Format: 99 (no commas, dollar signs)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['baths_three_quarter'] = array(
    '#type' => 'textfield',
    '#title' => t('Baths Three Quarter'),
    '#description' => t('Format: 99 (no commas, dollar signs)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['fireplace_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Fireplace Number'),
    '#description' => t('Format: 99 (no commas, dollar signs)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['total_rooms'] = array(
    '#type' => 'textfield',
    '#title' => t('Total Rooms'),
    '#description' => t('Format: 99 (no commas, dollar signs)'),
    '#size' => 4,
    '#maxlength' => 2,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['school_district'] = array(
    '#type' => 'textfield',
    '#title' => t('School District'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['school_elementary'] = array(
    '#type' => 'textfield',
    '#title' => t('School Elementary'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['school_middle'] = array(
    '#type' => 'textfield',
    '#title' => t('School Middle'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['school_junior_high'] = array(
    '#type' => 'textfield',
    '#title' => t('School Junior High'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['school_high'] = array(
    '#type' => 'textfield',
    '#title' => t('School High'),
    '#description' => t('Freeform'),
    '#size' => 32,
    '#maxlength' => 30,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['total_units'] = array(
    '#type' => 'textfield',
    '#title' => t('Total Units'),
    '#description' => t('Format: 99999 (no commas, dollar signs)'),
    '#size' => 7,
    '#maxlength' => 5,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['total_buildings'] = array(
    '#type' => 'textfield',
    '#title' => t('Total Buildings'),
    '#description' => t('Format: 999 (no commas, dollar signs)'),
    '#size' => 5,
    '#maxlength' => 3,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['total_lots'] = array(
    '#type' => 'textfield',
    '#title' => t('Total Lots'),
    '#description' => t('Format: 999 (no commas, dollar signs)'),
    '#size' => 5,
    '#maxlength' => 3,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['hoa_fees'] = array(
    '#type' => 'textfield',
    '#title' => t('HOA Fees'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['owners_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Owner\'s Name'),
    '#description' => t('Owner\'s Name'),
    '#size' => 52,
    '#maxlength' => 50,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['legal'] = array(
    '#type' => 'textarea',
    '#title' => t('Legal Description'),
    '#description' => t('750 character maximum'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['apn'] = array(
    '#type' => 'textfield',
    '#title' => t('APN'),
    '#description' => t('County Parcel ID or Tax Number'),
    '#size' => 47,
    '#maxlength' => 45,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['taxes'] = array(
    '#type' => 'textfield',
    '#title' => t('Taxes'),
    '#description' => t('Format: 999999 (no commas, dollar signs)'),
    '#size' => 8,
    '#maxlength' => 6,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['tax_year'] = array(
    '#type' => 'textfield',
    '#title' => t('Tax Year'),
    '#description' => t('Format: 9999 (no commas, dollar signs)'),
    '#size' => 6,
    '#maxlength' => 4,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['section'] = array(
    '#type' => 'textfield',
    '#title' => t('Section'),
    '#description' => t('Section'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['range'] = array(
    '#type' => 'textfield',
    '#title' => t('Range'),
    '#description' => t('Range'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['township'] = array(
    '#type' => 'textfield',
    '#title' => t('Township'),
    '#description' => t('Township'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['rent_on_season'] = array(
    '#type' => 'textfield',
    '#title' => t('Rent On Season'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['rent_off_season'] = array(
    '#type' => 'textfield',
    '#title' => t('Rent Off Season'),
    '#description' => t('Format: 9999999999 (no commas, dollar signs)'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['photo_ind'] = array(
    '#type' => 'textfield',
    '#title' => t('Photo Indicator'),
    '#description' => t('Is there a photo accompanying the proeprty \'X\' = Yes, Null = No'),
    '#size' => 3,
    '#maxlength' => 1,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['last_mls_update_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Last MLS Update Date'),
    '#description' => t('Format MM/DD/YYYY'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['master_bed'] = array(
    '#type' => 'textfield',
    '#title' => t('Master Bed'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['bed2'] = array(
    '#type' => 'textfield',
    '#title' => t('Bed 2'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['bed3'] = array(
    '#type' => 'textfield',
    '#title' => t('Bed 3'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['bed4'] = array(
    '#type' => 'textfield',
    '#title' => t('Bed 4'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['bed5'] = array(
    '#type' => 'textfield',
    '#title' => t('Bed 5'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['kitchen'] = array(
    '#type' => 'textfield',
    '#title' => t('Kitchen'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['breakfast'] = array(
    '#type' => 'textfield',
    '#title' => t('Breakfast'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['laundry'] = array(
    '#type' => 'textfield',
    '#title' => t('Laundry'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['den'] = array(
    '#type' => 'textfield',
    '#title' => t('Den'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['dining'] = array(
    '#type' => 'textfield',
    '#title' => t('Dining'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['family'] = array(
    '#type' => 'textfield',
    '#title' => t('Family'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['living'] = array(
    '#type' => 'textfield',
    '#title' => t('Living'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['great'] = array(
    '#type' => 'textfield',
    '#title' => t('Great'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['extra'] = array(
    '#type' => 'textfield',
    '#title' => t('Extra'),
    '#description' => t('Freeform: suggest Length x Width'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['general_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('General features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $general_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'A%'", $vid);
  while ($data = db_fetch_object($result)) {
    $general_feature_codes[] = $data->name;
  }
  $form['general_features']['general_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 2,
    '#options' => drupal_map_assoc($general_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['condition_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Condition features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $condition_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'B%'", $vid);
  while ($data = db_fetch_object($result)) {
    $condition_feature_codes[] = $data->name;
  }
  $form['condition_features']['condition_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($condition_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['community_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Community features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $community_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'C%'", $vid);
  while ($data = db_fetch_object($result)) {
    $community_feature_codes[] = $data->name;
  }
  $form['community_features']['community_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($community_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['condo_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Condo features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $condo_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('D01','D05','D06','D07','D09','D10','D11','D12','D19')", $vid);
  while ($data = db_fetch_object($result)) {
    $condo_feature_codes[] = $data->name;
  }
  $form['condo_features']['condo_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($condo_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['mobile_home_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile home features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $mobile_home_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('D13','D14','D15','D16','D17','D18')", $vid);
  while ($data = db_fetch_object($result)) {
    $mobile_home_feature_codes[] = $data->name;
  }
  $form['mobile_home_features']['mobile_home_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($mobile_home_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['construction_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Construction features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $construction_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'E%' AND `description` NOT IN ('E19','E20','E21','E22','E23','E23F')", $vid);
  while ($data = db_fetch_object($result)) {
    $construction_feature_codes[] = $data->name;
  }
  $form['construction_features']['construction_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($construction_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['roof_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roof features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $roof_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('E19','E20','E21','E22','E23','E23F')", $vid);
  while ($data = db_fetch_object($result)) {
    $roof_feature_codes[] = $data->name;
  }
  $form['roof_features']['roof_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 6,
    '#options' => drupal_map_assoc($roof_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['energy_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Energy features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $energy_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'F%'", $vid);
  while ($data = db_fetch_object($result)) {
    $energy_feature_codes[] = $data->name;
  }
  $form['energy_features']['energy_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($energy_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['exterior_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exterior features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $exterior_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'G%' AND `description` NOT IN ('G16','G17','G18','G19','G20','G21','G22','G23')", $vid);
  while ($data = db_fetch_object($result)) {
    $exterior_feature_codes[] = $data->name;
  }
  $form['exterior_features']['exterior_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 5,
    '#options' => drupal_map_assoc($exterior_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['fencing_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fencing features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $fencing_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('G16','G17','G18','G19','G20','G21','G22','G23')", $vid);
  while ($data = db_fetch_object($result)) {
    $fencing_feature_codes[] = $data->name;
  }
  $form['fencing_features']['fencing_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($fencing_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['financial_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Financial features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $financial_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'H%'", $vid);
  while ($data = db_fetch_object($result)) {
    $financial_feature_codes[] = $data->name;
  }
  $form['financial_features']['financial_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($financial_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['fireplaces_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fireplaces features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $fireplaces_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'I%'", $vid);
  while ($data = db_fetch_object($result)) {
    $fireplaces_feature_codes[] = $data->name;
  }
  $form['fireplaces_features']['fireplaces_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($fireplaces_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['flooring_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Flooring features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $flooring_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'J%'", $vid);
  while ($data = db_fetch_object($result)) {
    $flooring_feature_codes[] = $data->name;
  }
  $form['flooring_features']['flooring_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($flooring_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['foundation_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Foundation features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $foundation_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'K%'", $vid);
  while ($data = db_fetch_object($result)) {
    $foundation_feature_codes[] = $data->name;
  }
  $form['foundation_features']['foundation_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($foundation_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['laundry_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Laundry features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $laundry_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'L%'", $vid);
  while ($data = db_fetch_object($result)) {
    $laundry_feature_codes[] = $data->name;
  }
  $form['laundry_features']['laundry_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($laundry_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['heat_cool_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Heat/Cool features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $heat_cool_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'M%'", $vid);
  while ($data = db_fetch_object($result)) {
    $heat_cool_feature_codes[] = $data->name;
  }
  $form['heat_cool_features']['heat_cool_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($heat_cool_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['inclusions_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inclusions features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $inclusions_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'N%'", $vid);
  while ($data = db_fetch_object($result)) {
    $inclusions_feature_codes[] = $data->name;
  }
  $form['inclusions_features']['inclusions_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($inclusions_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['interior_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Interior features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $interior_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'O%'", $vid);
  while ($data = db_fetch_object($result)) {
    $interior_feature_codes[] = $data->name;
  }
  $form['interior_features']['interior_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($interior_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['land_use_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Land use features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $land_use_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'P%'", $vid);
  while ($data = db_fetch_object($result)) {
    $land_use_feature_codes[] = $data->name;
  }
  $form['land_use_features']['land_use_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($land_use_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['utilities_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Utilities features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $utilities_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'Q%'", $vid);
  while ($data = db_fetch_object($result)) {
    $utilities_feature_codes[] = $data->name;
  }
  $form['utilities_features']['utilities_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($utilities_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['location_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Location features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $location_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'R%'", $vid);
  while ($data = db_fetch_object($result)) {
    $location_feature_codes[] = $data->name;
  }
  $form['location_features']['location_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($location_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['lot_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lot features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $lot_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('S01','S02','S03','S04','S05','S06','S07','S08','S09','S10','S11','S12','S13','S14','S15','S16','S17','S18','S19','S20','S40','S47','S48','S49','S58','S59','S64','S65','S66','S67','S68','S69','S70','S71','S72','S73')", $vid);
  while ($data = db_fetch_object($result)) {
    $lot_feature_codes[] = $data->name;
  }
  $form['lot_features']['lot_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($lot_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['lot_size_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lot size features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $lot_size_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('S21','S22','S23','S24','S25','S26','S27','S28','S29','S30','S31','S32','S33','S34','S35','S36','S37','S38','S39','S41','S42','S43','S44','S45','S46')", $vid);
  while ($data = db_fetch_object($result)) {
    $lot_size_feature_codes[] = $data->name;
  }
  $form['lot_size_features']['lot_size_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($lot_size_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['parking_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parking features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $parking_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'T%'", $vid);
  while ($data = db_fetch_object($result)) {
    $parking_feature_codes[] = $data->name;
  }
  $form['parking_features']['parking_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($parking_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['recreation_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Recreation features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $recreation_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'U%'", $vid);
  while ($data = db_fetch_object($result)) {
    $recreation_feature_codes[] = $data->name;
  }
  $form['recreation_features']['recreation_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($recreation_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['rooms_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rooms features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $rooms_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'W%'", $vid);
  while ($data = db_fetch_object($result)) {
    $rooms_feature_codes[] = $data->name;
  }
  $form['rooms_features']['rooms_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 3,
    '#options' => drupal_map_assoc($rooms_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['scenery_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Scenery features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $scenery_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'X%'", $vid);
  while ($data = db_fetch_object($result)) {
    $scenery_feature_codes[] = $data->name;
  }
  $form['scenery_features']['scenery_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($scenery_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['stories_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stories features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $stories_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` IN ('Y01','Y02','Y03','Y04','Y05','Y06','Y07','Y08','Y09','Y10','Y50','Y51')", $vid);
  while ($data = db_fetch_object($result)) {
    $stories_feature_codes[] = $data->name;
  }
  $form['stories_features']['stories_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 5,
    '#options' => drupal_map_assoc($stories_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['style_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Style features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $style_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'Y%' AND `description` NOT IN ('Y01','Y02','Y03','Y04','Y05','Y06','Y07','Y08','Y09','Y10','Y50','Y51')", $vid);
  while ($data = db_fetch_object($result)) {
    $style_feature_codes[] = $data->name;
  }
  $form['style_features']['style_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($style_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['win_doors_features'] = array(
    '#type' => 'fieldset',
    '#title' => t('Win/Doors features'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => $weight,
  );
  $win_doors_feature_codes = array();
  $vid = db_result(db_query("SELECT `vid` FROM {vocabulary} WHERE `name` = 'Feature codes'"));
  $result = db_query("SELECT * FROM {term_data} WHERE `vid` = %d AND `description` LIKE 'Z%'", $vid);
  while ($data = db_fetch_object($result)) {
    $win_doors_feature_codes[] = $data->name;
  }
  $form['win_doors_features']['win_doors_feature_codes'] = array(
    '#type' => 'checkboxes',
    '#columns' => 4,
    '#options' => drupal_map_assoc($win_doors_feature_codes),
    '#theme' => 'relwmls_columns_checkboxes',
  );
//
  $weight += 1;
  $form['mls_office_id'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Office ID'),
    '#description' => t('MLS Office ID'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['mls_agent_id'] = array(
    '#type' => 'textfield',
    '#title' => t('MLS Agent ID'),
    '#description' => t('MLS Agent ID'),
    '#size' => 17,
    '#maxlength' => 15,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['virtual_tour_url'] = array(
    '#type' => 'textarea',
    '#title' => t('URL to Virtual Tour'),
    '#description' => t('200 character maximum'),
    '#cols' => 60,
    '#rows' => 5,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['photo_quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Photo Quantity'),
    '#description' => t('Total number of photos for property. Equal to 0 if no photos available for property.'),
    '#size' => 5,
    '#maxlength' => 3,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['photo_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Photo URL'),
    '#description' => t('Complete URL to access first photo of the property. Additional photos can be accessed by incrementing the filename portion of the URL of this first photo, up to the total number of photos contained in the PHOTO_QUANTITY column. For example if first photo\'s filename = 1234567.jpg, the 2nd photo would be 1234567_2.jpg, 3rd photo would be 1234567_3.jpg, etc. This methodology allows for directly linking to all photos for a given property. Empty if no photos available for property.'),
    '#size' => 102,
    '#maxlength' => 100,
    '#weight' => $weight,
  );
//
  $weight += 1;
  $form['photo_most_recent_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Photo Most Recent Date'),
    '#description' => t('Format MM/DD/YYYY'),
    '#size' => 12,
    '#maxlength' => 10,
    '#weight' => $weight,
  );
  return $form;
}

