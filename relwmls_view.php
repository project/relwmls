<?php

/**
 * @file
 *   View function for the relwmls module.
 * @ingroup relwmls
 */

/**
 * Implementation of hook_view()
 */
function relwmls_view($node, $teaser = FALSE, $page = FALSE) {
  if (!teaser) {
    $weight = 100;
    $node = node_prepare($node, $teaser);
    $node->content['mls_id'] = array(
      '#value' => theme('relwmls_mls_id', $node),
      '#weight' => $weight
    );
  }
  else {
    $node = node_prepare($node, $teaser);
  }
  return $node;
}

/**
 * theme_relwmls_mls_id()
 */
function theme_relwmls_mls_id($node) {
  $output = '<div class="relwmls_mls_id">'.
    check_markup($node->mls_id) .'</div><br />';
  return $output;
}

