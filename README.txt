
README.txt

This document describes procedures used to extract data and build functions
and files using the zip file from the Florida Regional (Orlando) Multiple
Listing Service. The file from your local board should be similar.

---

The midflregional.zip file unzips to give us:

IDX_ExportFileLayout.xls: an Excel file containing layout tables
midfl_data.txt: a comma-separated listings file
readme.txt: a file description file

The IDX_ExportFileLayout.xls file contains the following worksheets:
IDX_Property: field (column) definitions for the relwmls_mls_listings table
Status Codes: field (column) definitions for the status_codes vocabulary
Property Type Codes: field (column) definitions for the property_type_codes vocabulary
Feature Codes: field (column) definitions for the feature_codes vocabulary
County Codes: field (column) definitions for the county_codes vocabulary
City Codes: field (column) definitions for the city_codes vocabulary

---

Installing the relwmls module

Download and unzip the relwmls module as you would any other contributed module.
Install it under the sites/all/modules folder.
Enable it (Administer > Site building > Modules) from your Drupal admin menu.
Place the block (Administer > Site building > Blocks) in the Left sidebar.

---

Creating the .csv files

The IDX_ExportFileLayout.xls file will be used to generated .csv files. These
files are then used to generate the relwmls.install file automatically.

Status codes

Open the IDX_ExportFileLayout.xls spreadsheet.
Select the Status codes worksheet.
Click File > Save as.
Enter status_codes as the File name.
Select Text & Commas (.csv) as the Save as type.
Click Save, and then OK.
The status_codes.csv file is now in your Documents folder.
Copy it to your sites/default/files folder.

Property type codes

Open the IDX_ExportFileLayout.xls spreadsheet.
Select the Property type codes worksheet.
Click File > Save as.
Enter property_type_codes as the File name.
Select Text & Commas (.csv) as the Save as type.
Click Save, and then OK.
The property_type_codes.csv file is now in your Documents folder.
Copy it to your sites/default/files folder.

Feature codes

Open the IDX_ExportFileLayout.xls spreadsheet.
Select the Feature codes worksheet.
Click File > Save as.
Enter feature_codes as the File name.
Select Text & Commas (.csv) as the Save as type.
Click Save, and then OK.
The feature_codes.csv file is now in your Documents folder.
Copy it to your sites/default/files folder.

County codes

Open the IDX_ExportFileLayout.xls spreadsheet.
Select the County codes worksheet.
Click File > Save as.
Enter county_codes as the File name.
Select Text & Commas (.csv) as the Save as type.
Click Save, and then OK.
The county_codes.csv file is now in your Documents folder.
Copy it to your sites/default/files folder.

City codes

Open the IDX_ExportFileLayout.xls spreadsheet.
Select the City codes worksheet.
Click File > Save as.
Enter city_codes as the File name.
Select Text & Commas (.csv) as the Save as type.
Click Save, and then OK.
The city_codes.csv file is now in your Documents folder.
Copy it to your sites/default/files folder.

---

Generating the relwmls.install file

The relwmls.install file is generated automatically using the field (column)
definitions found in the IDX_ExportFileLayout worksheets.

The .csv files in the sites/default/files folder will be parsed, and the
table and vocabulary code will be generated.

Go to Administer > Site configuration > Relwmls configuration.
Check the "Generate install file" checkbox.
Click the Submit button.

If you now check the sites/modules/relwmls folder you will see that a relwmls.install
file has been created.

Go to Administer > Site building > Modules and disable the relwmls module.
Then enable it once again.

If you now check database (using the phpMyAdmin utility) you'll see that 5
vocabularies have been created, and their term_data entries have been made.
You will also see that the relwmls_mls_listing table has been created.

---

Importing the listing data

Go to Administer > Site configuration > Relwmls configuration.
Enter the name of the MLS data file, in this case midfl_data.txt.
Check the "Import the CSV file" checkbox.
Click the Submit button.

On a fast server the 88,295 records will be imported in less than 10 minutes.
On a slow server it may take hours.
Make some coffee.

Upon completion a message will appear saying that
"88,295 MLS listings imported (and inserted or updated)".

---

Viewing MLS listings

Set the minimum and maximum price specifications in the MLS listings block.
Click the "Search by city" link in the MLS listings block.
Check one or more cities to search.
Click the Submit button.

A page of listings will appear.
Click on the link at the bottom of a listing to see the MLS data.

You will see the MLS data for that property.

