<?php

/**
 * @file
 *   Validate function for the relwmls module.
 * @ingroup relwmls
 */

/**
 * Implementation of hook_validate()
 */
function relwmls_validate($node, &$form) {
  $feature_codes = '';
  foreach($form['general_features']['general_feature_codes']['#value'] as $element) {
    switch ($element) {
      case 'Handicap features':
        $feature_codes .= 'A01,';
        break;
      case 'Smoke-free':
        $feature_codes .= 'A02,';
        break;
      case 'Cats allowed':
        $feature_codes .= 'A03,';
        break;
      case 'Dogs allowed':
        $feature_codes .= 'A04,';
        break;
      case 'Pets allowed':
        $feature_codes .= 'A05,';
        break;
      case 'Pets on approval':
        $feature_codes .= 'A06,';
        break;
      case 'No pets allowed':
        $feature_codes .= 'A07,';
        break;
      case 'Pet restrictions':
        $feature_codes .= 'A08,';
        break;
      case 'Housing For Older Persons':
        $feature_codes .= 'A09,';
        break;
      case 'Seasonal residence':
        $feature_codes .= 'A10,';
        break;
      case 'Single Family (use if you can\'t map Property Type)':
        $feature_codes .= 'A11,';
        break;
      case 'Condo/Townhome (use if you can\'t map Property Type)':
        $feature_codes .= 'A12,';
        break;
      case 'Mobile home (use if you can\'t map Property Type)':
        $feature_codes .= 'A13,';
        break;
      case 'MultiFamily (use if you can\'t map Property Type)':
        $feature_codes .= 'A14,';
        break;
      case 'Farm (use if you can\'t map Property Type)':
        $feature_codes .= 'A15,';
        break;
      case 'Land (use if you can\'t map Property Type)':
        $feature_codes .= 'A16,';
        break;
      case 'Rental (use if you can\'t map Property Type)':
        $feature_codes .= 'A17,';
        break;
      case 'Government subsidized':
        $feature_codes .= 'A18,';
        break;
      case 'Sold ':
        $feature_codes .= 'B01,';
        break;
      case 'Sold ':
        $feature_codes .= 'B02,';
        break;
      case 'Fixer-upper':
        $feature_codes .= 'B03,';
        break;
      case 'currently under construction':
        $feature_codes .= 'B04,';
        break;
      case 'New construction':
        $feature_codes .= 'B05,';
        break;
      case 'Remodeled':
        $feature_codes .= 'B06,';
        break;
      case 'Home warranty (see agent for details)':
        $feature_codes .= 'B07,';
        break;
      case 'Newer fixtures':
        $feature_codes .= 'B08,';
        break;
      case 'Newer home (0-5 yrs)':
        $feature_codes .= 'B09,';
        break;
      case 'Newly painted':
        $feature_codes .= 'B10,';
        break;
      case 'Model':
        $feature_codes .= 'B11,';
        break;
      case 'To be built':
        $feature_codes .= 'B13,';
        break;
      case 'Community laundry facilities':
        $feature_codes .= 'C01,';
        break;
      case 'Pool table(s)':
        $feature_codes .= 'C02,';
        break;
      case 'Community boat facilities':
        $feature_codes .= 'C03,';
        break;
      case 'Clubhouse kitchen facilities':
        $feature_codes .= 'C04,';
        break;
      case 'Community clubhouse(s)':
        $feature_codes .= 'C05,';
        break;
      case 'Community exercise area(s)':
        $feature_codes .= 'C06,';
        break;
      case 'Community golf':
        $feature_codes .= 'C07,';
        break;
      case 'Handball':
        $feature_codes .= 'C08,';
        break;
      case 'Community horse facilities':
        $feature_codes .= 'C09,';
        break;
      case 'Access to riding trails':
        $feature_codes .= 'C10,';
        break;
      case 'Community in-door swimming pool':
        $feature_codes .= 'C11,';
        break;
      case 'Community park(s)':
        $feature_codes .= 'C12,';
        break;
      case 'Picnic area(s)':
        $feature_codes .= 'C13,';
        break;
      case 'Community children\'s play area':
        $feature_codes .= 'C14,';
        break;
      case 'Community swimming pool(s)':
        $feature_codes .= 'C15,';
        break;
      case 'Putting green':
        $feature_codes .= 'C16,';
        break;
      case 'Community sauna(s)':
        $feature_codes .= 'C17,';
        break;
      case 'Community spa(s)':
        $feature_codes .= 'C18,';
        break;
      case 'Community tennis court(s)':
        $feature_codes .= 'C19,';
        break;
      case 'Biking/fitness trail':
        $feature_codes .= 'C20,';
        break;
      case 'Gated community':
        $feature_codes .= 'C21,';
        break;
      case 'On-site guard':
        $feature_codes .= 'C22,';
        break;
      case 'Community security features':
        $feature_codes .= 'C23,';
        break;
      case 'Extra storage available':
        $feature_codes .= 'C24,';
        break;
      case 'Courtesy bus':
        $feature_codes .= 'C25,';
        break;
      case 'Close to public transportation':
        $feature_codes .= 'C27,';
        break;
      case 'First floor location':
        $feature_codes .= 'D01,';
        break;
      case 'Outside entrance':
        $feature_codes .= 'D05,';
        break;
      case 'End unit':
        $feature_codes .= 'D06,';
        break;
      case 'Ground floor unit':
        $feature_codes .= 'D07,';
        break;
      case 'Elevator(s)':
        $feature_codes .= 'D09,';
        break;
      case 'Individual electric meter':
        $feature_codes .= 'D10,';
        break;
      case 'Individual gas meter':
        $feature_codes .= 'D11,';
        break;
      case 'Private yard':
        $feature_codes .= 'D12,';
        break;
      case 'Includes foundation':
        $feature_codes .= 'D13,';
        break;
      case 'Single-wide mobile':
        $feature_codes .= 'D15,';
        break;
      case 'Double-wide mobile':
        $feature_codes .= 'D16,';
        break;
      case 'Triple-wide mobile':
        $feature_codes .= 'D17,';
        break;
      case 'Quadruple-wide mobile':
        $feature_codes .= 'D18,';
        break;
      case 'Resident manager':
        $feature_codes .= 'D19,';
        break;
      case 'Adobe construction':
        $feature_codes .= 'E01,';
        break;
      case 'Brick exterior':
        $feature_codes .= 'E02,';
        break;
      case 'Stucco and brick exterior':
        $feature_codes .= 'E03,';
        break;
      case 'Cedar/redwood siding':
        $feature_codes .= 'E04,';
        break;
      case 'Concrete block stucco':
        $feature_codes .= 'E05,';
        break;
      case 'Pier or beam foundation':
        $feature_codes .= 'E06,';
        break;
      case 'Frame and brick exterior':
        $feature_codes .= 'E07,';
        break;
      case 'Frame and stone exterior':
        $feature_codes .= 'E08,';
        break;
      case 'Frame and stucco exterior':
        $feature_codes .= 'E09,';
        break;
      case 'Log construction':
        $feature_codes .= 'E10,';
        break;
      case 'Modular construction':
        $feature_codes .= 'E11,';
        break;
      case 'Stone exterior':
        $feature_codes .= 'E12,';
        break;
      case 'Stucco exterior':
        $feature_codes .= 'E13,';
        break;
      case 'Vinyl/metal siding':
        $feature_codes .= 'E14,';
        break;
      case 'Vinyl siding':
        $feature_codes .= 'E15,';
        break;
      case 'Wood siding':
        $feature_codes .= 'E16,';
        break;
      case 'Wood shingle siding':
        $feature_codes .= 'E17,';
        break;
      case 'Frame exterior':
        $feature_codes .= 'E18,';
        break;
      case 'Composition roof':
        $feature_codes .= 'E19,';
        break;
      case 'Metal roof':
        $feature_codes .= 'E20,';
        break;
      case 'Slate roof':
        $feature_codes .= 'E21,';
        break;
      case 'Tile roof':
        $feature_codes .= 'E22,';
        break;
      case 'Shingle roof':
        $feature_codes .= 'E23,';
        break;
      case 'Wooden shake':
        $feature_codes .= 'E23F,';
        break;
      case 'Brick accent':
        $feature_codes .= 'E24,';
        break;
      case 'Masonite siding':
        $feature_codes .= 'E25,';
        break;
      case 'Water softner':
        $feature_codes .= 'F01,';
        break;
      case 'Two or more water heaters':
        $feature_codes .= 'F02,';
        break;
      case 'Solar water heater':
        $feature_codes .= 'F03,';
        break;
      case 'Programmable thermostat':
        $feature_codes .= 'F04,';
        break;
      case 'Zoned temperature control':
        $feature_codes .= 'F05,';
        break;
      case 'Insulated door(s)':
        $feature_codes .= 'F06,';
        break;
      case 'Insulated windows':
        $feature_codes .= 'F07,';
        break;
      case 'Storm windows':
        $feature_codes .= 'F08,';
        break;
      case 'Tinted windows':
        $feature_codes .= 'F09,';
        break;
      case 'Energy efficient rated home':
        $feature_codes .= 'F10,';
        break;
      case 'High energy furnace':
        $feature_codes .= 'F18,';
        break;
      case 'Aviary':
        $feature_codes .= 'G01,';
        break;
      case 'Balcony':
        $feature_codes .= 'G02,';
        break;
      case 'Balcony':
        $feature_codes .= 'G02,';
        break;
      case 'Carriage house':
        $feature_codes .= 'G03,';
        break;
      case 'Deck':
        $feature_codes .= 'G04,';
        break;
      case 'Dog run':
        $feature_codes .= 'G05,';
        break;
      case 'Gazebo':
        $feature_codes .= 'G06,';
        break;
      case 'Guest house':
        $feature_codes .= 'G07,';
        break;
      case 'Greenhouse':
        $feature_codes .= 'G08,';
        break;
      case 'Patio':
        $feature_codes .= 'G09,';
        break;
      case 'Covered patio':
        $feature_codes .= 'G10,';
        break;
      case 'Enclosed patio':
        $feature_codes .= 'G11,';
        break;
      case 'Covered porch':
        $feature_codes .= 'G12,';
        break;
      case 'Enclosed porch':
        $feature_codes .= 'G13,';
        break;
      case 'Storage/out-building(s)':
        $feature_codes .= 'G14,';
        break;
      case 'Outdoor lights':
        $feature_codes .= 'G15,';
        break;
      case 'Block fencing':
        $feature_codes .= 'G16,';
        break;
      case 'Fenced':
        $feature_codes .= 'G17,';
        break;
      case 'Partially fenced':
        $feature_codes .= 'G18,';
        break;
      case 'Wrought iron fencing':
        $feature_codes .= 'G19,';
        break;
      case 'Chain link fence':
        $feature_codes .= 'G20,';
        break;
      case 'Masonry fencing':
        $feature_codes .= 'G21,';
        break;
      case 'Security fence':
        $feature_codes .= 'G22,';
        break;
      case 'Wood fencing':
        $feature_codes .= 'G23,';
        break;
      case 'Barn':
        $feature_codes .= 'G25,';
        break;
      case 'Other farm buildings':
        $feature_codes .= 'G27,';
        break;
      case 'Porch':
        $feature_codes .= 'G29,';
        break;
      case 'Corporate owned':
        $feature_codes .= 'H01,';
        break;
      case 'See agent for details on association fees':
        $feature_codes .= 'H02,';
        break;
      case 'Home owners fee':
        $feature_codes .= 'H03,';
        break;
      case 'Hud owned':
        $feature_codes .= 'H04,';
        break;
      case 'Licensed owner':
        $feature_codes .= 'H05,';
        break;
      case 'Lender owned':
        $feature_codes .= 'H06,';
        break;
      case 'Lease option':
        $feature_codes .= 'H07,';
        break;
      case 'Lease/purchase':
        $feature_codes .= 'H08,';
        break;
      case 'No rentals':
        $feature_codes .= 'H09,';
        break;
      case 'Trade':
        $feature_codes .= 'H10,';
        break;
      case 'Zero downpayment':
        $feature_codes .= 'H11,';
        break;
      case 'Includes rental unit':
        $feature_codes .= 'H12,';
        break;
      case 'Cooperative ownership':
        $feature_codes .= 'H13,';
        break;
      case 'Foreclosure':
        $feature_codes .= 'H20,';
        break;
      case 'Short Sale':
        $feature_codes .= 'H21,';
        break;
      case 'Contingent':
        $feature_codes .= 'H22,';
        break;
      case 'Master bedroom fireplace':
        $feature_codes .= 'I01,';
        break;
      case 'Bedroom fireplace':
        $feature_codes .= 'I02,';
        break;
      case 'Basement fireplace':
        $feature_codes .= 'I03,';
        break;
      case 'Great room fireplace':
        $feature_codes .= 'I04,';
        break;
      case 'Dining room fireplace':
        $feature_codes .= 'I05,';
        break;
      case 'Kitchen fireplace':
        $feature_codes .= 'I06,';
        break;
      case 'Living room fireplace':
        $feature_codes .= 'I07,';
        break;
      case 'Family room fireplace':
        $feature_codes .= 'I08,';
        break;
      case 'Re-circulating fireplace':
        $feature_codes .= 'I09,';
        break;
      case 'Electric fireplace':
        $feature_codes .= 'I10,';
        break;
      case 'Freestanding fireplace':
        $feature_codes .= 'I11,';
        break;
      case 'Gas fireplace':
        $feature_codes .= 'I12,';
        break;
      case 'Fireplace(s)':
        $feature_codes .= 'I13,';
        break;
      case 'Multi-sided fireplace':
        $feature_codes .= 'I14,';
        break;
      case 'Gas fireplace logs':
        $feature_codes .= 'I15,';
        break;
      case 'Den fireplace':
        $feature_codes .= 'I16,';
        break;
      case 'Game room fireplace':
        $feature_codes .= 'I17,';
        break;
      case 'Fireplace insert':
        $feature_codes .= 'I18,';
        break;
      case 'Marble fireplace':
        $feature_codes .= 'I19,';
        break;
      case 'Stone fireplace':
        $feature_codes .= 'I20,';
        break;
      case 'Woodburning fireplace':
        $feature_codes .= 'I21,';
        break;
      case '2 fireplaces':
        $feature_codes .= 'I22,';
        break;
      case '3 or more fireplaces':
        $feature_codes .= 'I23,';
        break;
      case 'Wall to wall carpeting':
        $feature_codes .= 'J01,';
        break;
      case 'Carpet':
        $feature_codes .= 'J02,';
        break;
      case 'Clay flooring':
        $feature_codes .= 'J03,';
        break;
      case 'Marble floors':
        $feature_codes .= 'J04,';
        break;
      case 'Parquet floors':
        $feature_codes .= 'J05,';
        break;
      case 'Terrazo floors':
        $feature_codes .= 'J06,';
        break;
      case 'Tile flooring':
        $feature_codes .= 'J07,';
        break;
      case 'Vinyl flooring':
        $feature_codes .= 'J08,';
        break;
      case 'Hardwood floors':
        $feature_codes .= 'J09,';
        break;
      case 'Newer carpet':
        $feature_codes .= 'J10,';
        break;
      case 'Dining room is carpeted':
        $feature_codes .= 'J11,';
        break;
      case 'Dining room has tile floor':
        $feature_codes .= 'J12,';
        break;
      case 'Dining room has wood floor':
        $feature_codes .= 'J13,';
        break;
      case 'Family room is carpeted':
        $feature_codes .= 'J14,';
        break;
      case 'Family room has tile floor':
        $feature_codes .= 'J15,';
        break;
      case 'Family room has wood floor':
        $feature_codes .= 'J16,';
        break;
      case 'Recreation room has brick floor':
        $feature_codes .= 'J17,';
        break;
      case 'Recreation room is carpeted':
        $feature_codes .= 'J18,';
        break;
      case 'Recreation room has tile floor':
        $feature_codes .= 'J19,';
        break;
      case 'Recreation room has wood floor':
        $feature_codes .= 'J20,';
        break;
      case 'Great room has brick floor':
        $feature_codes .= 'J21,';
        break;
      case 'Great room is carpeted':
        $feature_codes .= 'J22,';
        break;
      case 'Great room has tile floor':
        $feature_codes .= 'J23,';
        break;
      case 'Great room has wood floor':
        $feature_codes .= 'J24,';
        break;
      case 'Kitchen has brick floor':
        $feature_codes .= 'J25,';
        break;
      case 'Kitchen is carpeted':
        $feature_codes .= 'J26,';
        break;
      case 'Kitchen has tile floor':
        $feature_codes .= 'J27,';
        break;
      case 'Kitchen has vinyl floor':
        $feature_codes .= 'J28,';
        break;
      case 'Kitchen has wood floor':
        $feature_codes .= 'J29,';
        break;
      case 'Living room has carpeting':
        $feature_codes .= 'J30,';
        break;
      case 'Living room has tile floor':
        $feature_codes .= 'J31,';
        break;
      case 'Living room has wood floor':
        $feature_codes .= 'J32,';
        break;
      case 'Brick floors':
        $feature_codes .= 'J33,';
        break;
      case 'Brick flooring':
        $feature_codes .= 'J33,';
        break;
      case 'Granite floors':
        $feature_codes .= 'J34,';
        break;
      case 'Slate floors':
        $feature_codes .= 'J35,';
        break;
      case 'Wood floors':
        $feature_codes .= 'J39,';
        break;
      case 'Cellar':
        $feature_codes .= 'K01,';
        break;
      case 'Slab foundation':
        $feature_codes .= 'K02,';
        break;
      case 'Partial basement':
        $feature_codes .= 'K03,';
        break;
      case 'Partially finished basement':
        $feature_codes .= 'K04,';
        break;
      case 'Finished basement':
        $feature_codes .= 'K05,';
        break;
      case 'Walk-out basement':
        $feature_codes .= 'K06,';
        break;
      case 'Basement':
        $feature_codes .= 'K07,';
        break;
      case 'Basement bedroom':
        $feature_codes .= 'K08,';
        break;
      case 'Electric laundry hookups':
        $feature_codes .= 'L01,';
        break;
      case 'Gas laundry hookups':
        $feature_codes .= 'L02,';
        break;
      case 'Washer/dryer hookups':
        $feature_codes .= 'L03,';
        break;
      case 'Laundry sink':
        $feature_codes .= 'L04,';
        break;
      case 'Laundry shute':
        $feature_codes .= 'L05,';
        break;
      case 'Laundry closet':
        $feature_codes .= 'L06,';
        break;
      case 'Laundry room/area':
        $feature_codes .= 'L07,';
        break;
      case 'Laundry area in garage/carport':
        $feature_codes .= 'L08,';
        break;
      case 'Laundry area in kitchen':
        $feature_codes .= 'L09,';
        break;
      case 'Main floor laundry':
        $feature_codes .= 'L10,';
        break;
      case 'Upper floor laundry':
        $feature_codes .= 'L11,';
        break;
      case 'Central air conditioning':
        $feature_codes .= 'M01,';
        break;
      case 'Swamp cooler':
        $feature_codes .= 'M02,';
        break;
      case 'Wall or window air conditioner(s)':
        $feature_codes .= 'M03,';
        break;
      case 'House fan':
        $feature_codes .= 'M04,';
        break;
      case 'Central heat':
        $feature_codes .= 'M05,';
        break;
      case 'Forced air heat':
        $feature_codes .= 'M06,';
        break;
      case 'Floor furnace':
        $feature_codes .= 'M07,';
        break;
      case 'Convection heating':
        $feature_codes .= 'M08,';
        break;
      case 'Hot water heat':
        $feature_codes .= 'M09,';
        break;
      case 'Radiators':
        $feature_codes .= 'M10,';
        break;
      case 'Radiant heat':
        $feature_codes .= 'M11,';
        break;
      case 'Baseboard heat':
        $feature_codes .= 'M12,';
        break;
      case 'Wall heating unit(s)':
        $feature_codes .= 'M13,';
        break;
      case 'Wood stove':
        $feature_codes .= 'M14,';
        break;
      case 'Heat pump':
        $feature_codes .= 'M15,';
        break;
      case 'Solar heating features':
        $feature_codes .= 'M16,';
        break;
      case 'Electric heating':
        $feature_codes .= 'M17,';
        break;
      case 'Gas heating':
        $feature_codes .= 'M18,';
        break;
      case 'Propane heating':
        $feature_codes .= 'M19,';
        break;
      case 'Electric air filter':
        $feature_codes .= 'M20,';
        break;
      case 'Natural gas water heater':
        $feature_codes .= 'M21,';
        break;
      case 'Humidifier':
        $feature_codes .= 'M22,';
        break;
      case 'Oil heating':
        $feature_codes .= 'M23,';
        break;
      case 'Multiple fuel sources':
        $feature_codes .= 'M24,';
        break;
      case 'Central air conditioning in unit':
        $feature_codes .= 'M34,';
        break;
      case 'Central air conditioning in building':
        $feature_codes .= 'M35,';
        break;
      case 'Central heat in unit':
        $feature_codes .= 'M36,';
        break;
      case 'Central heat in building':
        $feature_codes .= 'M37,';
        break;
      case 'Gas range and oven':
        $feature_codes .= 'N01,';
        break;
      case 'Electric range and oven':
        $feature_codes .= 'N02,';
        break;
      case 'Self-cleaning oven':
        $feature_codes .= 'N03,';
        break;
      case 'Double oven':
        $feature_codes .= 'N04,';
        break;
      case 'Downdraft oven':
        $feature_codes .= 'N05,';
        break;
      case 'Stove':
        $feature_codes .= 'N06,';
        break;
      case 'Microwave oven':
        $feature_codes .= 'N07,';
        break;
      case 'Cooktop range':
        $feature_codes .= 'N08,';
        break;
      case 'Indoor grill':
        $feature_codes .= 'N09,';
        break;
      case 'Disposal':
        $feature_codes .= 'N10,';
        break;
      case 'Dishwasher':
        $feature_codes .= 'N11,';
        break;
      case 'Ice maker line':
        $feature_codes .= 'N12,';
        break;
      case 'Refrigerator':
        $feature_codes .= 'N13,';
        break;
      case 'Sub-zero refrigerator':
        $feature_codes .= 'N14,';
        break;
      case 'Bar refrigerator':
        $feature_codes .= 'N15,';
        break;
      case 'Freezer':
        $feature_codes .= 'N16,';
        break;
      case 'Trash compactor':
        $feature_codes .= 'N17,';
        break;
      case 'Clothes washer':
        $feature_codes .= 'N18,';
        break;
      case 'Clothes dryer':
        $feature_codes .= 'N19,';
        break;
      case 'Water filtration system':
        $feature_codes .= 'N20,';
        break;
      case 'Built-in oven':
        $feature_codes .= 'N21,';
        break;
      case 'Stacked washer/dryer':
        $feature_codes .= 'N22,';
        break;
      case 'Exhaust fan/hood':
        $feature_codes .= 'N23,';
        break;
      case 'Range and oven':
        $feature_codes .= 'N24,';
        break;
      case 'Furnished':
        $feature_codes .= 'N29,';
        break;
      case 'Partially furnished':
        $feature_codes .= 'N30,';
        break;
      case 'Country kitchen':
        $feature_codes .= 'O01,';
        break;
      case 'Breakfast bar':
        $feature_codes .= 'O02,';
        break;
      case 'Pantry':
        $feature_codes .= 'O03,';
        break;
      case 'Kitchen isle':
        $feature_codes .= 'O04,';
        break;
      case 'Jetted bathtub':
        $feature_codes .= 'O05,';
        break;
      case 'Tub and separate shower':
        $feature_codes .= 'O06,';
        break;
      case 'Dry bar':
        $feature_codes .= 'O07,';
        break;
      case 'Wetbar':
        $feature_codes .= 'O08,';
        break;
      case 'Ceiling fan(s)':
        $feature_codes .= 'O09,';
        break;
      case 'Intercom system':
        $feature_codes .= 'O10,';
        break;
      case 'Central vacuum system':
        $feature_codes .= 'O11,';
        break;
      case 'Vaulted ceilings':
        $feature_codes .= 'O12,';
        break;
      case 'Skylight(s)':
        $feature_codes .= 'O13,';
        break;
      case 'Walk-in closet(s)':
        $feature_codes .= 'O14,';
        break;
      case 'Window treatments':
        $feature_codes .= 'O15,';
        break;
      case 'Security features':
        $feature_codes .= 'O16,';
        break;
      case 'Fire sprinkler system':
        $feature_codes .= 'O17,';
        break;
      case 'Open floor plan':
        $feature_codes .= 'O18,';
        break;
      case 'Limited living area':
        $feature_codes .= 'O19,';
        break;
      case 'Secondary bedrooms split from master bedroom':
        $feature_codes .= 'O20,';
        break;
      case 'Built-in features':
        $feature_codes .= 'O21,';
        break;
      case 'Cathedral ceilings':
        $feature_codes .= 'O22,';
        break;
      case 'Cedar closet(s)':
        $feature_codes .= 'O23,';
        break;
      case 'His & Hers closets':
        $feature_codes .= 'O24,';
        break;
      case 'Closet organizer':
        $feature_codes .= 'O25,';
        break;
      case 'Wall to wall closets':
        $feature_codes .= 'O26,';
        break;
      case 'Decorator kitchen counters':
        $feature_codes .= 'O27,';
        break;
      case 'Modern kitchen':
        $feature_codes .= 'O28,';
        break;
      case 'Decorator lighting':
        $feature_codes .= 'O29,';
        break;
      case 'Mirrored closet(s)':
        $feature_codes .= 'O30,';
        break;
      case 'Built-in desk':
        $feature_codes .= 'O31,';
        break;
      case 'Built-in bookcase(s)/shelve(s)':
        $feature_codes .= 'O32,';
        break;
      case 'Entertainment center':
        $feature_codes .= 'O33,';
        break;
      case 'Farm land':
        $feature_codes .= 'P01,';
        break;
      case 'Farm or ranch':
        $feature_codes .= 'P02,';
        break;
      case 'Grove(s)':
        $feature_codes .= 'P03,';
        break;
      case 'Horse(s) allowed':
        $feature_codes .= 'P04,';
        break;
      case 'Orchard(s)':
        $feature_codes .= 'P05,';
        break;
      case 'Ranch land':
        $feature_codes .= 'P06,';
        break;
      case 'Resort land':
        $feature_codes .= 'P07,';
        break;
      case 'Water rights':
        $feature_codes .= 'P08,';
        break;
      case 'Tillable acres':
        $feature_codes .= 'P09,';
        break;
      case 'Pasture':
        $feature_codes .= 'P10,';
        break;
      case 'Timber acres':
        $feature_codes .= 'P11,';
        break;
      case 'Cattle farm':
        $feature_codes .= 'P12,';
        break;
      case 'Dairy farm':
        $feature_codes .= 'P13,';
        break;
      case 'Hobby farm':
        $feature_codes .= 'P14,';
        break;
      case 'Hog farm':
        $feature_codes .= 'P15,';
        break;
      case 'Horse farm':
        $feature_codes .= 'P16,';
        break;
      case 'Nursery farm':
        $feature_codes .= 'P17,';
        break;
      case 'Poultry farm':
        $feature_codes .= 'P18,';
        break;
      case 'Row crops farming':
        $feature_codes .= 'P19,';
        break;
      case 'Sheep farm':
        $feature_codes .= 'P20,';
        break;
      case 'Tree farm':
        $feature_codes .= 'P21,';
        break;
      case 'Auto watering system':
        $feature_codes .= 'P24,';
        break;
      case 'Other farm equipment':
        $feature_codes .= 'P28,';
        break;
      case 'Vineyard':
        $feature_codes .= 'P29,';
        break;
      case 'Grazing land':
        $feature_codes .= 'P30,';
        break;
      case 'Irrigated farmland':
        $feature_codes .= 'P31,';
        break;
      case 'Public sewer service':
        $feature_codes .= 'Q01,';
        break;
      case 'Septic sewer system':
        $feature_codes .= 'Q02,';
        break;
      case 'Well pump':
        $feature_codes .= 'Q03,';
        break;
      case 'Public water supply':
        $feature_codes .= 'Q04,';
        break;
      case 'Private water supply':
        $feature_codes .= 'Q05,';
        break;
      case 'Water supply from well(s)':
        $feature_codes .= 'Q06,';
        break;
      case 'TV Antenna':
        $feature_codes .= 'Q07,';
        break;
      case 'TV cable available':
        $feature_codes .= 'Q08,';
        break;
      case 'Satellite dish':
        $feature_codes .= 'Q09,';
        break;
      case 'Water supply on-site':
        $feature_codes .= 'Q10,';
        break;
      case 'Water supply at street':
        $feature_codes .= 'Q11,';
        break;
      case 'Water supply near by':
        $feature_codes .= 'Q12,';
        break;
      case 'Private sewer service':
        $feature_codes .= 'Q13,';
        break;
      case 'Sewer service on-site':
        $feature_codes .= 'Q14,';
        break;
      case 'Sewer service at street':
        $feature_codes .= 'Q15,';
        break;
      case 'Sewer service near by':
        $feature_codes .= 'Q16,';
        break;
      case 'Sewer service available':
        $feature_codes .= 'Q17,';
        break;
      case 'Gas service on-site':
        $feature_codes .= 'Q18,';
        break;
      case 'Gas service at street':
        $feature_codes .= 'Q19,';
        break;
      case 'Gas service near by':
        $feature_codes .= 'Q20,';
        break;
      case 'Natural gas service available':
        $feature_codes .= 'Q21,';
        break;
      case 'Electric service on-site':
        $feature_codes .= 'Q22,';
        break;
      case 'Electric service at street':
        $feature_codes .= 'Q23,';
        break;
      case 'Electric service near by':
        $feature_codes .= 'Q24,';
        break;
      case 'Electric service underground':
        $feature_codes .= 'Q25,';
        break;
      case 'Electric service available':
        $feature_codes .= 'Q26,';
        break;
      case 'Telephone service available':
        $feature_codes .= 'Q27,';
        break;
      case 'Rent includes water':
        $feature_codes .= 'Q28,';
        break;
      case 'Rent includes gas':
        $feature_codes .= 'Q29,';
        break;
      case 'Rent includes electric':
        $feature_codes .= 'Q30,';
        break;
      case 'Rent includes sewer':
        $feature_codes .= 'Q31,';
        break;
      case 'Rent includes garbage':
        $feature_codes .= 'Q32,';
        break;
      case 'Out of city limits':
        $feature_codes .= 'R01,';
        break;
      case 'Rural property':
        $feature_codes .= 'R02,';
        break;
      case 'Located in subdvision':
        $feature_codes .= 'R03,';
        break;
      case 'Eastern exposure':
        $feature_codes .= 'R04,';
        break;
      case 'Northeastern exposure':
        $feature_codes .= 'R05,';
        break;
      case 'Northern exposure':
        $feature_codes .= 'R06,';
        break;
      case 'Northwestern exposure':
        $feature_codes .= 'R07,';
        break;
      case 'Southeastern exposure':
        $feature_codes .= 'R08,';
        break;
      case 'Southern exposure':
        $feature_codes .= 'R09,';
        break;
      case 'Southwestern exposure':
        $feature_codes .= 'R10,';
        break;
      case 'Western exposure':
        $feature_codes .= 'R11,';
        break;
      case 'Located in historical district':
        $feature_codes .= 'R12,';
        break;
      case 'Land lease':
        $feature_codes .= 'S01,';
        break;
      case 'Level lot':
        $feature_codes .= 'S02,';
        break;
      case 'Rolling lot':
        $feature_codes .= 'S03,';
        break;
      case 'Sloped lot':
        $feature_codes .= 'S04,';
        break;
      case 'Zero lot line':
        $feature_codes .= 'S05,';
        break;
      case 'Patio home lot':
        $feature_codes .= 'S06,';
        break;
      case 'Corner lot':
        $feature_codes .= 'S07,';
        break;
      case 'Cul-de-sac':
        $feature_codes .= 'S08,';
        break;
      case 'Trees':
        $feature_codes .= 'S09,';
        break;
      case 'Wooded':
        $feature_codes .= 'S10,';
        break;
      case 'Rock out-croppings':
        $feature_codes .= 'S11,';
        break;
      case 'Pond(s)':
        $feature_codes .= 'S12,';
        break;
      case 'Adjacent to farm land':
        $feature_codes .= 'S13,';
        break;
      case 'Adjacent to National Forest':
        $feature_codes .= 'S14,';
        break;
      case 'Adjacent to meadow':
        $feature_codes .= 'S15,';
        break;
      case 'Adjacent to public or private open area':
        $feature_codes .= 'S16,';
        break;
      case 'Adjacent to river/stream or creek':
        $feature_codes .= 'S17,';
        break;
      case 'Landscape sprinklers':
        $feature_codes .= 'S18,';
        break;
      case 'Professionally landscaped':
        $feature_codes .= 'S19,';
        break;
      case 'Irrigation well':
        $feature_codes .= 'S20,';
        break;
      case 'Lot size is 1/4 acre or less':
        $feature_codes .= 'S21,';
        break;
      case 'Lot size between 1/4 and 1/2 acre':
        $feature_codes .= 'S22,';
        break;
      case 'Lot size between 1/2 and 3/4 acre':
        $feature_codes .= 'S23,';
        break;
      case 'Lots size between 3/4 and 1 acre':
        $feature_codes .= 'S24,';
        break;
      case 'Lot size between 3/4 and 1 1/2 acre':
        $feature_codes .= 'S25,';
        break;
      case 'Less than 1 acre':
        $feature_codes .= 'S26,';
        break;
      case 'Lot size is between 1 and 2 acres':
        $feature_codes .= 'S27,';
        break;
      case 'Lots size between 1 1/2 and 2 1/2 acres':
        $feature_codes .= 'S28,';
        break;
      case 'Lot size between 2 and 3 acres':
        $feature_codes .= 'S29,';
        break;
      case 'Lot size between 2 1/2 and 5 acres':
        $feature_codes .= 'S30,';
        break;
      case 'Lot size between 3 and 5 acres':
        $feature_codes .= 'S31,';
        break;
      case 'Lot size is between 1 and 5 acres':
        $feature_codes .= 'S32,';
        break;
      case 'Lot size is between 5 and 10 acres':
        $feature_codes .= 'S33,';
        break;
      case 'Lot size of 10 or more acres':
        $feature_codes .= 'S34,';
        break;
      case 'Lot size is between 10 and 20 acres':
        $feature_codes .= 'S35,';
        break;
      case 'Lot size is 20 or more acres':
        $feature_codes .= 'S36,';
        break;
      case 'Lot size between 20 and 35 acres':
        $feature_codes .= 'S37,';
        break;
      case 'Lot size is 35 or more acres':
        $feature_codes .= 'S38,';
        break;
      case 'Lot size is 5 or more acres':
        $feature_codes .= 'S39,';
        break;
      case 'Landscaping':
        $feature_codes .= 'S40,';
        break;
      case 'Lot size is less than 1/2 acre':
        $feature_codes .= 'S41,';
        break;
      case 'Lot size is between 1/2 and 1 acre':
        $feature_codes .= 'S42,';
        break;
      case 'Lot size is between 1 and 3 acres':
        $feature_codes .= 'S43,';
        break;
      case 'Lot size is between 2 and 5 acres':
        $feature_codes .= 'S44,';
        break;
      case 'Lot size is between 2 and 3 acres':
        $feature_codes .= 'S45,';
        break;
      case 'Lot size is between 10 and 15 acres':
        $feature_codes .= 'S46,';
        break;
      case 'Clear lot':
        $feature_codes .= 'S47,';
        break;
      case 'Partially cleared lot':
        $feature_codes .= 'S48,';
        break;
      case 'Oversized lot':
        $feature_codes .= 'S49,';
        break;
      case 'Interior lot':
        $feature_codes .= 'S58,';
        break;
      case 'Irregular':
        $feature_codes .= 'S59,';
        break;
      case 'Curbs & gutters':
        $feature_codes .= 'S64,';
        break;
      case 'Storm drains':
        $feature_codes .= 'S65,';
        break;
      case 'Sidewalk':
        $feature_codes .= 'S66,';
        break;
      case 'Rough graded lot':
        $feature_codes .= 'S67,';
        break;
      case 'Finish graded lot':
        $feature_codes .= 'S68,';
        break;
      case 'Staked lot':
        $feature_codes .= 'S69,';
        break;
      case 'Elevated lot':
        $feature_codes .= 'S70,';
        break;
      case 'Wetlands':
        $feature_codes .= 'S71,';
        break;
      case 'Additional land available':
        $feature_codes .= 'S72,';
        break;
      case 'Divisible lot':
        $feature_codes .= 'S73,';
        break;
      case '1 parking place':
        $feature_codes .= 'T01,';
        break;
      case '2 parking places':
        $feature_codes .= 'T02,';
        break;
      case '3 or more parking places':
        $feature_codes .= 'T03,';
        break;
      case '1 carport':
        $feature_codes .= 'T04,';
        break;
      case '2 car carport':
        $feature_codes .= 'T05,';
        break;
      case '3 or more car carport':
        $feature_codes .= 'T06,';
        break;
      case '1 car garage':
        $feature_codes .= 'T07,';
        break;
      case '2 car garage':
        $feature_codes .= 'T08,';
        break;
      case '3 car garage':
        $feature_codes .= 'T09,';
        break;
      case '4 or more car garage':
        $feature_codes .= 'T10,';
        break;
      case 'Assigned parking':
        $feature_codes .= 'T11,';
        break;
      case 'Attached parking':
        $feature_codes .= 'T12,';
        break;
      case 'Detached parking':
        $feature_codes .= 'T13,';
        break;
      case 'Parking garage':
        $feature_codes .= 'T14,';
        break;
      case 'Circular driveway':
        $feature_codes .= 'T15,';
        break;
      case 'Covered parking area':
        $feature_codes .= 'T16,';
        break;
      case 'Guest parking':
        $feature_codes .= 'T17,';
        break;
      case 'Parking pad':
        $feature_codes .= 'T18,';
        break;
      case 'Underground parking':
        $feature_codes .= 'T19,';
        break;
      case 'RV parking':
        $feature_codes .= 'T20,';
        break;
      case 'Valet parking':
        $feature_codes .= 'T21,';
        break;
      case 'Converted garage':
        $feature_codes .= 'T22,';
        break;
      case 'Automatic garage door':
        $feature_codes .= 'T23,';
        break;
      case 'Heated garage':
        $feature_codes .= 'T24,';
        break;
      case 'Oversized garage':
        $feature_codes .= 'T25,';
        break;
      case 'Carport':
        $feature_codes .= 'T26,';
        break;
      case 'Garage':
        $feature_codes .= 'T27,';
        break;
      case 'Space(s)':
        $feature_codes .= 'T28,';
        break;
      case 'Storage in parking area':
        $feature_codes .= 'T29,';
        break;
      case 'Garage apartment':
        $feature_codes .= 'T30,';
        break;
      case 'Brick driveway':
        $feature_codes .= 'T31,';
        break;
      case 'Paved driveway':
        $feature_codes .= 'T32,';
        break;
      case 'Stone driveway':
        $feature_codes .= 'T33,';
        break;
      case 'Basement garage':
        $feature_codes .= 'T34,';
        break;
      case 'Tandem garage':
        $feature_codes .= 'T35,';
        break;
      case 'swimming pool':
        $feature_codes .= 'U01,';
        break;
      case 'Indoor swimming pool':
        $feature_codes .= 'U02,';
        break;
      case 'Heated pool':
        $feature_codes .= 'U03,';
        break;
      case 'In-ground swimming pool':
        $feature_codes .= 'U04,';
        break;
      case 'Saltwater swimming pool':
        $feature_codes .= 'U05,';
        break;
      case 'In-ground, screen enclosed swimming pool':
        $feature_codes .= 'U06,';
        break;
      case 'Solar heated pool':
        $feature_codes .= 'U07,';
        break;
      case 'Lap pool':
        $feature_codes .= 'U08,';
        break;
      case 'Above ground pool':
        $feature_codes .= 'U09,';
        break;
      case 'Automatic pool chlorination':
        $feature_codes .= 'U10,';
        break;
      case 'Automatic pool cleaner':
        $feature_codes .= 'U11,';
        break;
      case 'Fenced pool area':
        $feature_codes .= 'U12,';
        break;
      case 'Spa':
        $feature_codes .= 'U13,';
        break;
      case 'Indoor spa':
        $feature_codes .= 'U14,';
        break;
      case 'Outdoor spa':
        $feature_codes .= 'U15,';
        break;
      case 'Sauna':
        $feature_codes .= 'U16,';
        break;
      case 'Outdoor gas grill':
        $feature_codes .= 'U17,';
        break;
      case 'BBQ':
        $feature_codes .= 'U18,';
        break;
      case 'Boat facilities':
        $feature_codes .= 'U19,';
        break;
      case 'Boat dockage available':
        $feature_codes .= 'U20,';
        break;
      case 'Boat dockage':
        $feature_codes .= 'U21,';
        break;
      case 'Tennis court':
        $feature_codes .= 'U22,';
        break;
      case 'Horse facilities':
        $feature_codes .= 'U23,';
        break;
      case 'Cabana':
        $feature_codes .= 'U24,';
        break;
      case 'Private beach':
        $feature_codes .= 'U25,';
        break;
      case 'Family room':
        $feature_codes .= 'W01,';
        break;
      case 'Family room in basement':
        $feature_codes .= 'W02,';
        break;
      case 'Family room on lower level':
        $feature_codes .= 'W03,';
        break;
      case 'Family room on main floor':
        $feature_codes .= 'W04,';
        break;
      case 'Family room upstairs':
        $feature_codes .= 'W05,';
        break;
      case 'Formal dining room':
        $feature_codes .= 'W06,';
        break;
      case 'Formal living room':
        $feature_codes .= 'W07,';
        break;
      case 'L-shaped living/dining room combination':
        $feature_codes .= 'W08,';
        break;
      case 'Living/dining room combination':
        $feature_codes .= 'W09,';
        break;
      case 'Kitchen/dining room':
        $feature_codes .= 'W10,';
        break;
      case 'Breakfast area':
        $feature_codes .= 'W11,';
        break;
      case 'Master bedroom suite':
        $feature_codes .= 'W12,';
        break;
      case 'Master bathroom':
        $feature_codes .= 'W13,';
        break;
      case 'Master bedroom is downstairs':
        $feature_codes .= 'W14,';
        break;
      case 'Master bedroom on lower level':
        $feature_codes .= 'W15,';
        break;
      case 'Master bedroom on main floor':
        $feature_codes .= 'W16,';
        break;
      case 'Master bedroom upstairs':
        $feature_codes .= 'W17,';
        break;
      case '2nd master bedroom':
        $feature_codes .= 'W18,';
        break;
      case 'Guest room':
        $feature_codes .= 'W19,';
        break;
      case 'In-law suite':
        $feature_codes .= 'W20,';
        break;
      case 'Convertible room':
        $feature_codes .= 'W21,';
        break;
      case 'Room addition':
        $feature_codes .= 'W22,';
        break;
      case 'Extra room':
        $feature_codes .= 'W23,';
        break;
      case 'Den':
        $feature_codes .= 'W24,';
        break;
      case 'Office':
        $feature_codes .= 'W25,';
        break;
      case 'Game room':
        $feature_codes .= 'W26,';
        break;
      case 'Great room':
        $feature_codes .= 'W27,';
        break;
      case 'Exercise room':
        $feature_codes .= 'W28,';
        break;
      case 'Loft':
        $feature_codes .= 'W29,';
        break;
      case 'Foyer':
        $feature_codes .= 'W30,';
        break;
      case 'Finished attic':
        $feature_codes .= 'W31,';
        break;
      case 'Attic':
        $feature_codes .= 'W32,';
        break;
      case 'Library':
        $feature_codes .= 'W33,';
        break;
      case 'Storage room':
        $feature_codes .= 'W34,';
        break;
      case 'Maid or guest suite':
        $feature_codes .= 'W35,';
        break;
      case 'Sun room':
        $feature_codes .= 'W36,';
        break;
      case 'Utility room':
        $feature_codes .= 'W37,';
        break;
      case 'Wine cellar':
        $feature_codes .= 'W38,';
        break;
      case 'Workshop':
        $feature_codes .= 'W39,';
        break;
      case 'Sewing room':
        $feature_codes .= 'W40,';
        break;
      case 'Play room':
        $feature_codes .= 'W41,';
        break;
      case 'Atrium':
        $feature_codes .= 'W42,';
        break;
      case 'Breezeway':
        $feature_codes .= 'W43,';
        break;
      case 'Dressing area':
        $feature_codes .= 'W44,';
        break;
      case 'Eat in kitchen':
        $feature_codes .= 'W45,';
        break;
      case 'His & Hers Master baths':
        $feature_codes .= 'W46,';
        break;
      case 'Music room':
        $feature_codes .= 'W47,';
        break;
      case 'Nursery':
        $feature_codes .= 'W48,';
        break;
      case 'Sitting room':
        $feature_codes .= 'W49,';
        break;
      case 'Dining area':
        $feature_codes .= 'W54,';
        break;
      case 'Lake access':
        $feature_codes .= 'X01,';
        break;
      case 'Lake view':
        $feature_codes .= 'X02,';
        break;
      case 'Lake frontage':
        $feature_codes .= 'X03,';
        break;
      case 'Lake or river view':
        $feature_codes .= 'X04,';
        break;
      case 'River frontage/Access':
        $feature_codes .= 'X05,';
        break;
      case 'River view':
        $feature_codes .= 'X06,';
        break;
      case 'Ocean view':
        $feature_codes .= 'X07,';
        break;
      case 'Ocean access':
        $feature_codes .= 'X08,';
        break;
      case 'Ocean frontage':
        $feature_codes .= 'X09,';
        break;
      case 'Bay or ocean frontage':
        $feature_codes .= 'X10,';
        break;
      case 'Bay view':
        $feature_codes .= 'X11,';
        break;
      case 'Waterfront property':
        $feature_codes .= 'X12,';
        break;
      case 'Canal view':
        $feature_codes .= 'X13,';
        break;
      case 'Canal waterfront property':
        $feature_codes .= 'X14,';
        break;
      case 'Lagoon view':
        $feature_codes .= 'X15,';
        break;
      case 'Intra-coastal':
        $feature_codes .= 'X16,';
        break;
      case 'Valley/canyon view':
        $feature_codes .= 'X17,';
        break;
      case 'Swimming pool view':
        $feature_codes .= 'X18,';
        break;
      case 'Clubhouse view':
        $feature_codes .= 'X19,';
        break;
      case 'Tennis Court view':
        $feature_codes .= 'X20,';
        break;
      case 'Golf course frontage':
        $feature_codes .= 'X21,';
        break;
      case 'Golf course view':
        $feature_codes .= 'X22,';
        break;
      case 'Garden view':
        $feature_codes .= 'X23,';
        break;
      case 'Greenbelt view':
        $feature_codes .= 'X24,';
        break;
      case 'Hill country':
        $feature_codes .= 'X25,';
        break;
      case 'Hill/mountain view':
        $feature_codes .= 'X26,';
        break;
      case 'Back range view':
        $feature_codes .= 'X27,';
        break;
      case 'Foothills view':
        $feature_codes .= 'X28,';
        break;
      case 'Panoramic Views':
        $feature_codes .= 'X29,';
        break;
      case 'Park-like views':
        $feature_codes .= 'X30,';
        break;
      case 'View of the plains':
        $feature_codes .= 'X31,';
        break;
      case 'Adjacent to park':
        $feature_codes .= 'X32,';
        break;
      case 'City lights view':
        $feature_codes .= 'X33,';
        break;
      case 'Scenic view':
        $feature_codes .= 'X34,';
        break;
      case 'Waterview':
        $feature_codes .= 'X35,';
        break;
      case 'Gulf frontage':
        $feature_codes .= 'X36F,';
        break;
      case 'Gulf view':
        $feature_codes .= 'X37F,';
        break;
      case 'Gulf access':
        $feature_codes .= 'X38F,';
        break;
      case 'Single story':
        $feature_codes .= 'Y01,';
        break;
      case 'Story and a half':
        $feature_codes .= 'Y02,';
        break;
      case 'Two story':
        $feature_codes .= 'Y03,';
        break;
      case 'Three story':
        $feature_codes .= 'Y04,';
        break;
      case 'Four story':
        $feature_codes .= 'Y05,';
        break;
      case 'Bi-level':
        $feature_codes .= 'Y06,';
        break;
      case 'Multi-story':
        $feature_codes .= 'Y07,';
        break;
      case 'Multi-level':
        $feature_codes .= 'Y08,';
        break;
      case 'Tri-level with basement':
        $feature_codes .= 'Y09,';
        break;
      case 'Tri-level':
        $feature_codes .= 'Y10,';
        break;
      case 'A-frame':
        $feature_codes .= 'Y11,';
        break;
      case 'Cabin':
        $feature_codes .= 'Y12,';
        break;
      case 'Cape Cod':
        $feature_codes .= 'Y13,';
        break;
      case 'Chalet style':
        $feature_codes .= 'Y14,';
        break;
      case 'Colonial':
        $feature_codes .= 'Y15,';
        break;
      case 'Contemporary':
        $feature_codes .= 'Y16,';
        break;
      case 'Cottage/bungalow style':
        $feature_codes .= 'Y17,';
        break;
      case 'Country style':
        $feature_codes .= 'Y18,';
        break;
      case 'Denver Square style':
        $feature_codes .= 'Y19,';
        break;
      case 'Duplex':
        $feature_codes .= 'Y20,';
        break;
      case 'Earth berm':
        $feature_codes .= 'Y21,';
        break;
      case 'Farm house style':
        $feature_codes .= 'Y22,';
        break;
      case 'Highrise':
        $feature_codes .= 'Y23,';
        break;
      case 'Penthouse':
        $feature_codes .= 'Y24,';
        break;
      case 'Ranch':
        $feature_codes .= 'Y25,';
        break;
      case 'Raised ranch style':
        $feature_codes .= 'Y26,';
        break;
      case 'Spanish':
        $feature_codes .= 'Y27,';
        break;
      case 'Studio/efficiency':
        $feature_codes .= 'Y28,';
        break;
      case 'Traditional':
        $feature_codes .= 'Y29,';
        break;
      case 'Tudor':
        $feature_codes .= 'Y30,';
        break;
      case 'Victorian':
        $feature_codes .= 'Y31,';
        break;
      case 'Townhouse style':
        $feature_codes .= 'Y32,';
        break;
      case 'Cluster style':
        $feature_codes .= 'Y33,';
        break;
      case 'Dutch colonial style':
        $feature_codes .= 'Y34,';
        break;
      case 'Split foyer style':
        $feature_codes .= 'Y35,';
        break;
      case 'Georgian style':
        $feature_codes .= 'Y36,';
        break;
      case 'Williamsburg style':
        $feature_codes .= 'Y37,';
        break;
      case 'Split-level':
        $feature_codes .= 'Y38,';
        break;
      case 'Bungalow style':
        $feature_codes .= 'Y39,';
        break;
      case 'Santa Fe style':
        $feature_codes .= 'Y40,';
        break;
      case 'Condo':
        $feature_codes .= 'Y41,';
        break;
      case 'C+C601oop condominium':
        $feature_codes .= 'Y42,';
        break;
      case 'English style':
        $feature_codes .= 'Y43,';
        break;
      case 'Mediterranean style':
        $feature_codes .= 'Y44,';
        break;
      case 'Oriental style':
        $feature_codes .= 'Y45,';
        break;
      case 'Western style':
        $feature_codes .= 'Y46,';
        break;
      case 'Time share':
        $feature_codes .= 'Y48,';
        break;
      case 'Single level':
        $feature_codes .= 'Y50,';
        break;
      case 'Two and a half story':
        $feature_codes .= 'Y51,';
        break;
      case 'Four-plex':
        $feature_codes .= 'Y61,';
        break;
      case 'French provincial':
        $feature_codes .= 'Y62,';
        break;
      case 'Historic':
        $feature_codes .= 'Y63,';
        break;
      case 'Multiunit':
        $feature_codes .= 'Y67,';
        break;
      case 'Triplex':
        $feature_codes .= 'Y71,';
        break;
      case 'Two family':
        $feature_codes .= 'Y73,';
        break;
      case 'Three family':
        $feature_codes .= 'Y74,';
        break;
      case 'Four family':
        $feature_codes .= 'Y75,';
        break;
      case 'Five or more family':
        $feature_codes .= 'Y76,';
        break;
      case 'Residential':
        $feature_codes .= 'Y77,';
        break;
      case 'Commercial':
        $feature_codes .= 'Y78,';
        break;
      case 'Agricultural':
        $feature_codes .= 'Y79,';
        break;
      case 'Industrial':
        $feature_codes .= 'Y80,';
        break;
      case 'Short term rental':
        $feature_codes .= 'Y81,';
        break;
      case 'Bay window(s)':
        $feature_codes .= 'Z01,';
        break;
      case 'French door(s)':
        $feature_codes .= 'Z02,';
        break;
      case 'Hurricane awnings':
        $feature_codes .= 'Z03,';
        break;
      case 'Plantation shutters':
        $feature_codes .= 'Z04,';
        break;
      case 'Awnings':
        $feature_codes .= 'Z05,';
        break;
      case 'Greenhouse window':
        $feature_codes .= 'Z06,';
        break;
      case 'Sliding glass door(s)':
        $feature_codes .= 'Z07,';
        break;
      case 'Stained glass window':
        $feature_codes .= 'Z08,';
        break;
      case 'Jalousie windows':
        $feature_codes .= 'Z09,';
        break;
      case 'Picture window':
        $feature_codes .= 'Z10,';
        break;
      case 'Shutters':
        $feature_codes .= 'Z20,';
        break;
    }
  }
  $_SESSION['relwmls_feature_codes'] = $feature_codes;
}

